Aviva Techinical Test

1. Compile jar using maven:
mvn clean compile test surefire-report:report  package

2. Example command to run program (Parameter is the input schedule information that is to be run):
java -cp AvivaBooking-0.0.1-SNAPSHOT.jar com.ntak.examples.AvivaBooking.App ../src/main/resource/input.txt
