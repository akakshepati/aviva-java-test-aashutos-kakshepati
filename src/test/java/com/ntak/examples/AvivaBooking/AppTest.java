package com.ntak.examples.AvivaBooking;

import java.io.FileNotFoundException;

import org.junit.Test;

import com.ntak.examples.AvivaBooking.io.exception.DataAccessException;

/**
 * Unit test for simple App.
 */
public class AppTest {

	@Test
	public void testNormalInput() throws FileNotFoundException, DataAccessException {
		App.main(new String[]{"./src/main/resource/input.txt"});
	}
	
	@Test
	public void testNoInputHandled() throws FileNotFoundException, DataAccessException {
		App.main(new String[]{});
	}
	
	@Test
	public void testNullInputHandled() throws FileNotFoundException, DataAccessException {
		App.main(new String[]{null});
	}
	
	@Test
	public void testEmptyInputHandled() throws FileNotFoundException, DataAccessException {
		App.main(new String[]{""});
	}
	
	@Test
	public void testSpaceInputHandled() throws FileNotFoundException, DataAccessException {
		App.main(new String[]{""});
	}
	
}
