package com.ntak.examples.AvivaBooking.model.beans;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.junit.Test;

public class MeetingTest {

	// Too large duration bound (>=24 hours)
	@Test
	public void testDuration24Hrs_DefaultValues() {
		Meeting meeting = new Meeting(LocalDate.now(),LocalTime.parse("09:00"),24,"EMPLOYEE1",LocalDateTime.now());
		
		assertNotNull(meeting);
		assertEquals(LocalTime.MIDNIGHT,meeting.getStartTime());
		assertEquals(LocalTime.of(1,0),meeting.getEndTime());
	}
	
	
	@Test
	public void testDurationGrtThn24Hrs_DefaultValues() {
		Meeting meeting = new Meeting(LocalDate.now(),LocalTime.parse("09:00"),25,"EMPLOYEE1",LocalDateTime.now());
		
		assertNotNull(meeting);
		assertEquals(LocalTime.MIDNIGHT,meeting.getStartTime());
		assertEquals(LocalTime.of(1,0),meeting.getEndTime());
	}
	
	
	// Negative duration
	@Test
	public void testNegativeDuration_DefaultValues() {
		Meeting meeting = new Meeting(LocalDate.now(),LocalTime.parse("10:00"),-1,"EMPLOYEE1",LocalDateTime.now());
		
		assertNotNull(meeting);
		assertEquals(LocalTime.MIDNIGHT,meeting.getStartTime());
		assertEquals(LocalTime.of(1,0),meeting.getEndTime());
	}
	
	// 0 duration
	@Test
	public void testZeroDuration_DefaultValues() {
		Meeting meeting = new Meeting(LocalDate.now(),LocalTime.parse("10:00"),0,"EMPLOYEE1",LocalDateTime.now());
		
		assertNotNull(meeting);
		assertEquals(LocalTime.MIDNIGHT,meeting.getStartTime());
		assertEquals(LocalTime.of(1,0),meeting.getEndTime());
	}
	
	// Null Start Time
	@Test
	public void testNullStartTime_DefaultValues() {
		Meeting meeting = new Meeting(LocalDate.now(),null,1,"EMPLOYEE1",LocalDateTime.now());
		
		assertNotNull(meeting);
		assertEquals(LocalTime.MIDNIGHT,meeting.getStartTime());
		assertEquals(LocalTime.of(1,0),meeting.getEndTime());
	}
	
	// Null Entry Time
	@Test
	public void testNullSubmissionTime_DefaultValues() {
		Meeting meeting = new Meeting(LocalDate.now(),LocalTime.parse("10:00"),1,"EMPLOYEE1",null);
		
		assertNotNull(meeting);
		assertEquals(LocalTime.of(10,0),meeting.getStartTime());
		assertEquals(LocalTime.of(11,0),meeting.getEndTime());
		assertNotNull(meeting.getSubmissionTime());
	}
	
	// Null EmployeeId => Get System User information
	@Test
	public void testNullEmployeeId_DefaultValues() {
		Meeting meeting = new Meeting(LocalDate.now(),LocalTime.parse("10:00"),1,null,LocalDateTime.now());
		
		assertNotNull(meeting);
		assertEquals(LocalTime.of(10,0),meeting.getStartTime());
		assertEquals(LocalTime.of(11,0),meeting.getEndTime());
		assertNotNull(meeting.getEmployeeId());
		assertEquals(System.getProperty("user.name"),meeting.getEmployeeId());
		System.out.println("User details: " + System.getProperty("user.name"));
	}
	
	// Normal meeting case - 1 hour
	@Test
	public void testNValid1hrMeeting_DefaultValues() {
		LocalDateTime timestamp = LocalDateTime.now();
		Meeting meeting = new Meeting(LocalDate.now(),LocalTime.parse("10:00"),1,"EMPLOYEE1",timestamp);
		
		assertNotNull(meeting);
		assertEquals(LocalDate.now(),meeting.getDate());
		assertEquals(LocalTime.of(10,0),meeting.getStartTime());
		assertEquals(LocalTime.of(11,0),meeting.getEndTime());
		assertEquals(1,meeting.getDuration());
		assertNotNull(meeting.getEmployeeId());
		assertEquals("EMPLOYEE1",meeting.getEmployeeId());
		
		assertEquals(timestamp,meeting.getSubmissionTime());
	}
	
	
	// Normal meeting case - 4 hour
	@Test
	public void testNValid4hrMeeting_DefaultValues() {
		LocalDateTime timestamp = LocalDateTime.now();
		Meeting meeting = new Meeting(LocalDate.now(),LocalTime.parse("10:00"),4,"EMPLOYEE1",timestamp);
		
		assertNotNull(meeting);
		assertEquals(LocalDate.now(),meeting.getDate());
		assertEquals(LocalTime.of(10,0),meeting.getStartTime());
		assertEquals(LocalTime.of(14,0),meeting.getEndTime());
		assertEquals(4,meeting.getDuration());
		assertNotNull(meeting.getEmployeeId());
		assertEquals("EMPLOYEE1",meeting.getEmployeeId());
		
		assertEquals(timestamp,meeting.getSubmissionTime());
	}
	
	@Test
	public void meetingAfterAnotherMeetingSameDate_Return1() {
		LocalDateTime timestamp = LocalDateTime.now();
		Meeting meeting = new Meeting(LocalDate.now(),LocalTime.parse("10:00"),1,"EMPLOYEE32",timestamp);
		Meeting otherMeeting = new Meeting(LocalDate.now(),LocalTime.parse("09:00"),1,"EMPLOYEE32",timestamp);
		assertEquals(1,meeting.compareTo(otherMeeting));
	}
	
	@Test
	public void meetingBeforeAnotherMeetingSameDate_ReturnNeg1() {
		LocalDateTime timestamp = LocalDateTime.now();
		Meeting meeting = new Meeting(LocalDate.now(),LocalTime.parse("09:00"),1,"EMPLOYEE32",timestamp);
		Meeting otherMeeting = new Meeting(LocalDate.now(),LocalTime.parse("10:00"),1,"EMPLOYEE32",timestamp);
		assertEquals(-1,meeting.compareTo(otherMeeting));
	}
	
	@Test
	public void meetingAfterAnotherMeetingDifferentDate_Return1() {
		LocalDateTime timestamp = LocalDateTime.now();
		Meeting meeting = new Meeting(LocalDate.now().plusDays(1),LocalTime.parse("10:00"),1,"EMPLOYEE32",timestamp);
		Meeting otherMeeting = new Meeting(LocalDate.now(),LocalTime.parse("09:00"),1,"EMPLOYEE32",timestamp);
		assertEquals(1,meeting.compareTo(otherMeeting));
	}
	
	@Test
	public void meetingBeforeAnotherMeetingDifferentDate_ReturnNeg1() {
		LocalDateTime timestamp = LocalDateTime.now();
		Meeting meeting = new Meeting(LocalDate.now(),LocalTime.parse("10:00"),1,"EMPLOYEE32",timestamp);
		Meeting otherMeeting = new Meeting(LocalDate.now().plusDays(1),LocalTime.parse("09:00"),1,"EMPLOYEE32",timestamp);
		assertEquals(-1,meeting.compareTo(otherMeeting));	
	}
	
	@Test
	public void meetingAtSameTimeAsAnotherMeeting_Returns0() {
		LocalDateTime timestamp = LocalDateTime.now();
		Meeting meeting = new Meeting(LocalDate.now(),LocalTime.parse("10:00"),1,"EMPLOYEE32",timestamp);
		Meeting otherMeeting = new Meeting(LocalDate.now(),LocalTime.parse("10:00"),1,"EMPLOYEE32",timestamp);
		assertEquals(0,meeting.compareTo(otherMeeting));	
	}
	
	@Test
	public void testPrintRecordMethodOutputsAsExpected() {
		LocalDateTime timestamp = LocalDateTime.now();
		Meeting meeting = new Meeting(LocalDate.now(),LocalTime.parse("10:00"),1,"EMPLOYEE32",timestamp);
		assertEquals("10:00 11:00 EMPLOYEE32",meeting.printRecord());
	}
}
