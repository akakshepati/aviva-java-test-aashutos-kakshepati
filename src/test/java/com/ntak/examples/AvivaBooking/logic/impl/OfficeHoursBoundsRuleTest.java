package com.ntak.examples.AvivaBooking.logic.impl;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.LinkedList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ntak.examples.AvivaBooking.model.beans.Meeting;

public class OfficeHoursBoundsRuleTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testNullAllConstructor_DefaultValuesInitialised() {
		OfficeHoursBoundsRule rule = new OfficeHoursBoundsRule(null,null);
		assertNotNull(rule);
		assertEquals(LocalTime.parse("09:00"),rule.getOpeningTime());
		assertEquals(LocalTime.parse("18:00"),rule.getClosingTime());
	}
	
	@Test
	public void testNullOpeningConstructor_DefaultValuesInitialised() {
		OfficeHoursBoundsRule rule = new OfficeHoursBoundsRule(null,LocalTime.parse("22:00"));
		assertNotNull(rule);
		assertEquals(LocalTime.parse("09:00"),rule.getOpeningTime());
		assertEquals(LocalTime.parse("18:00"),rule.getClosingTime());
	}
	
	@Test
	public void testNullClosingConstructor_DefaultValuesInitialised() {
		OfficeHoursBoundsRule rule = new OfficeHoursBoundsRule(LocalTime.parse("11:00"),null);
		assertNotNull(rule);
		assertEquals(LocalTime.parse("09:00"),rule.getOpeningTime());
		assertEquals(LocalTime.parse("18:00"),rule.getClosingTime());
	}
	
	@Test
	public void testNoParamConstructor_DefaultValuesInitialised() {
		OfficeHoursBoundsRule rule = new OfficeHoursBoundsRule();
		assertNotNull(rule);
		assertEquals(LocalTime.parse("09:00"),rule.getOpeningTime());
		assertEquals(LocalTime.parse("18:00"),rule.getClosingTime());
	}
	
	@Test
	public void testParamConstructor_InitialisedAsExpected() {
		OfficeHoursBoundsRule rule = new OfficeHoursBoundsRule(LocalTime.parse("08:30"),LocalTime.parse("17:30"));
		assertNotNull(rule);
		assertEquals(LocalTime.parse("08:30"),rule.getOpeningTime());
		assertEquals(LocalTime.parse("17:30"),rule.getClosingTime());
	}
	
	@Test
	public void testInvalidDateRangeParamConstructor_InitialisedAsExpected() {
		OfficeHoursBoundsRule rule = new OfficeHoursBoundsRule(LocalTime.parse("22:30"),LocalTime.parse("00:30"));
		assertNotNull(rule);
		assertEquals(LocalTime.parse("09:00"),rule.getOpeningTime());
		assertEquals(LocalTime.parse("18:00"),rule.getClosingTime());
	}
	
	@Test
	public void testSameDateRangeParamValuesConstructor_InitialisedAsExpected() {
		OfficeHoursBoundsRule rule = new OfficeHoursBoundsRule(LocalTime.parse("08:00"),LocalTime.parse("08:00"));
		assertNotNull(rule);
		assertEquals(LocalTime.parse("09:00"),rule.getOpeningTime());
		assertEquals(LocalTime.parse("18:00"),rule.getClosingTime());
	}
	
	// Valid meeting timeframe
	
	@Test
	public void testValidMeeting_ReturnsTrue() {
		OfficeHoursBoundsRule rule = new OfficeHoursBoundsRule();
		assertTrue(rule.checkValid(new Meeting(LocalDate.now(),LocalTime.parse("12:00"),2,"EMPLOYEE1",LocalDateTime.now()),new LinkedList<Meeting>()));
	}
	
	@Test
	public void testValidMeetingOnClose_ReturnsTrue() {
		OfficeHoursBoundsRule rule = new OfficeHoursBoundsRule();
		assertTrue(rule.checkValid(new Meeting(LocalDate.now(),LocalTime.parse("16:00"),2,"EMPLOYEE1",LocalDateTime.now()),new LinkedList<Meeting>()));
	}

	@Test
	public void testValidMeetingOnOpen_ReturnsTrue() {
		OfficeHoursBoundsRule rule = new OfficeHoursBoundsRule();
		assertTrue(rule.checkValid(new Meeting(LocalDate.now(),LocalTime.parse("09:00"),2,"EMPLOYEE1",LocalDateTime.now()),new LinkedList<Meeting>()));
	}	
	
	// Invalid meeting timeframe

	@Test
	public void testInvalidMeetingStartAfterClose_ReturnsFalse() {
		OfficeHoursBoundsRule rule = new OfficeHoursBoundsRule();
		assertFalse(rule.checkValid(new Meeting(LocalDate.now(),LocalTime.parse("22:00"),2,"EMPLOYEE1",LocalDateTime.now()),new LinkedList<Meeting>()));
	}
	
	@Test
	public void testInvalidMeetingStartBeforeOpen_ReturnsFalse() {
		OfficeHoursBoundsRule rule = new OfficeHoursBoundsRule();
		assertFalse(rule.checkValid(new Meeting(LocalDate.now(),LocalTime.parse("05:00"),2,"EMPLOYEE1",LocalDateTime.now()),new LinkedList<Meeting>()));
	}
	
	@Test
	public void testInvalidMeetingEndAfterClose_ReturnsFalse() {
		OfficeHoursBoundsRule rule = new OfficeHoursBoundsRule();
		assertFalse(rule.checkValid(new Meeting(LocalDate.now(),LocalTime.parse("17:00"),2,"EMPLOYEE1",LocalDateTime.now()),new LinkedList<Meeting>()));
	}
	
	@Test
	public void testNullMeetingBeforeOpen_ReturnsFalse() {
		OfficeHoursBoundsRule rule = new OfficeHoursBoundsRule();
		assertFalse(rule.checkValid(null,new LinkedList<Meeting>()));
	}
}
