package com.ntak.examples.AvivaBooking.logic.impl;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ntak.examples.AvivaBooking.model.beans.Meeting;

public class NoMeetingClashRuleTest {

	private List<Meeting> meetings = new LinkedList<Meeting>();
	private NoMeetingClashRule rule = new NoMeetingClashRule();
	
	@Before
	public void setUp() throws Exception {
		meetings.add(new Meeting(LocalDate.now(),LocalTime.parse("11:00"),1,"EMPLOYEE1",LocalDateTime.now()));
		meetings.add(new Meeting(LocalDate.now(),LocalTime.parse("15:00"),1,"EMPLOYEE2",LocalDateTime.now()));
	}

	@After
	public void tearDown() throws Exception {
		meetings.clear();
	}
	
	// Meeting after another meeting - Valid
	@Test
	public void testSuccessiveNonOverlappingMeetingFoundLater_ReturnsTrue() {
		assertTrue(rule.checkValid(new Meeting(LocalDate.now(),LocalTime.parse("17:00"),1,"EMPLOYEE76",LocalDateTime.now()), meetings));
	}
	
	// Meeting before another meeting - Valid
	@Test
	public void testSuccessiveNonOverlappingMeetingFoundBefore_ReturnsTrue() {
		assertTrue(rule.checkValid(new Meeting(LocalDate.now(),LocalTime.parse("09:00"),1,"EMPLOYEE117",LocalDateTime.now()), meetings));
	}
	
	// Loose sandwich meeting - Valid
	@Test
	public void testLooseSandwichMeeting_ReturnsTrue() {
		assertTrue(rule.checkValid(new Meeting(LocalDate.now(),LocalTime.parse("13:00"),1,"EMPLOYEE99",LocalDateTime.now()), meetings));
	}
	
	
	// Packed sandwich meeting - Valid
	@Test
	public void testPackedSandwichMeeting_ReturnsTrue() {
		assertTrue(rule.checkValid(new Meeting(LocalDate.now(),LocalTime.parse("12:00"),3,"EMPLOYEE121",LocalDateTime.now()), meetings));
	}
	
	// On the dot before - Valid
	@Test
	public void testOnTheDotBeforeMeeting_ReturnsTrue() {
		assertTrue(rule.checkValid(new Meeting(LocalDate.now(),LocalTime.parse("10:00"),1,"EMPLOYEE32",LocalDateTime.now()), meetings));
	}
	
	// On the dot after - Valid
	@Test
	public void testOnTheDotAfterMeeting_ReturnsTrue() {
		assertTrue(rule.checkValid(new Meeting(LocalDate.now(),LocalTime.parse("16:00"),2,"EMPLOYEE32",LocalDateTime.now()), meetings));
	}
	
	// Null list of existing meetings
	@Test
	public void testNullSetOfMeetings_ReturnsTrue() {
		assertTrue(rule.checkValid(new Meeting(LocalDate.now(),LocalTime.parse("15:00"),2,"EMPLOYEE65",LocalDateTime.now()), null));
	}
	
	// Empty list
	@Test
	public void testNoMeetings_ReturnsTrue() {
		assertTrue(rule.checkValid(new Meeting(LocalDate.now(),LocalTime.parse("15:00"),2,"EMPLOYEE100",LocalDateTime.now()), new LinkedList<Meeting>()));
	}
	
	// Null meeting in list
	@Test
	public void testNullMeetingInMeetings_ReturnsTrue() {
		meetings.add(null);
		assertTrue(rule.checkValid(new Meeting(LocalDate.now(),LocalTime.parse("09:00"),1,"EMPLOYEE107",LocalDateTime.now()), meetings));
	}
	
	// Null meeting to add
	@Test
	public void testAddMeetingToPureNullList_ReturnsTrue() {
		meetings.clear();
		meetings.add(null);
		assertTrue(rule.checkValid(new Meeting(LocalDate.now(),LocalTime.parse("15:00"),2,"EMPLOYEE105",LocalDateTime.now()), meetings));
	}
	
	// Before valid. After invalid.
	@Test
	public void testOverlappingEndTime_ReturnsFalse() {
		assertFalse(rule.checkValid(new Meeting(LocalDate.now(),LocalTime.parse("14:00"),2,"EMPLOYEE110",LocalDateTime.now()), meetings));
	}
	
	// Before invalid. After valid.
	@Test
	public void testOverlappingStartTime_ReturnsFalse() {
		assertFalse(rule.checkValid(new Meeting(LocalDate.now(),LocalTime.parse("15:00"),2,"EMPLOYEE115",LocalDateTime.now()), meetings));
	}
	
	// Null meeting to add
	@Test
	public void testCheckNullMeeting_ReturnsFalse() {
		assertFalse(rule.checkValid(null, meetings));
	}
}
