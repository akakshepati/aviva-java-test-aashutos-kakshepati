package com.ntak.examples.AvivaBooking.logic.impl;

import java.util.List;

import com.ntak.examples.AvivaBooking.logic.Rule;
import com.ntak.examples.AvivaBooking.model.beans.Meeting;

public class DummyRule implements Rule {

	private static int noOfInvocations = 0;
	private boolean dumRetVal = false;
	
	public DummyRule() {
		super();
	}
	
	public DummyRule(boolean retVal) {
		dumRetVal = retVal;
	}

	@Override
	public boolean checkValid(Meeting newMeet, List<Meeting> meetings) {
		noOfInvocations++;
		return dumRetVal;
	}

	public static int getNoOfInvocations() {
		return noOfInvocations;
	}
	
	public static void resetCounter() {
		noOfInvocations = 0;
	}
}
