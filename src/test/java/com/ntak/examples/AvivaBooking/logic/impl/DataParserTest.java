package com.ntak.examples.AvivaBooking.logic.impl;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

import junit.framework.TestCase;

public class DataParserTest extends TestCase {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	@Test
	public void testParseDateISOFormat_ReturnsLocalDate() {
		assertEquals(LocalDate.parse("2011-01-01"), DataParser.parseDate("2011-01-01", "yyyy-MM-dd"));
	}
	
	@Test
	public void testParseDateValidDateValidFormat_ReturnsLocalDate() {
		assertEquals(LocalDate.parse("2011-01-01"), DataParser.parseDate("01/01/2011", "dd/MM/yyyy"));
	}
	
	
	@Test
	public void testParseDateNullFormat_ReturnsDefaultValue() {
		assertEquals(LocalDate.MIN, DataParser.parseDate("01/01/2011", null));
	}
	
	@Test
	public void testParseDateNullDateString_ReturnsDefaultValue() {
		assertEquals(LocalDate.MIN, DataParser.parseDate(null, "dd/MM/yyyy"));
	}

	@Test
	public void testParseDateNullParams_ReturnsDefaultValue() {
		assertEquals(LocalDate.MIN, DataParser.parseDate(null, null));
	}
	
	@Test
	public void testParseDateInvalidMonth_ReturnsDefaultValue() {
		assertEquals(LocalDate.MIN, DataParser.parseDate("2016-13-01", "yyyy-MM-dd"));
	}
	
	@Test
	public void testParseDateInvalidDay_ReturnsDefaultValue() {
		assertEquals(LocalDate.MIN, DataParser.parseDate("2016-10-44", "yyyy-MM-dd"));
	}
	
	@Test
	public void testParseDateInvalidFormat_ReturnsDefaultValue() {
		assertEquals(LocalDate.MIN, DataParser.parseDate("2016-10-01", "abcdefg"));
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	@Test
	public void testParseTimeISOFormat_ReturnsLocalDate() {
		assertEquals(LocalTime.parse("10:00:00"),DataParser.parseTime("10:00:00", "HH:mm:ss"));
	}
	
	@Test
	public void testParseTimeValidTimeValidFormat_ReturnsLocalDate() {
		assertEquals(LocalTime.parse("10:00:00.000"),DataParser.parseTime("10:00:00.000", "HH:mm:ss.SSS"));
	}
	
	@Test
	public void testParseTimeNullFormat_ReturnsDefaultValue() {
		assertEquals(LocalTime.MIN, DataParser.parseTime("08:00:00", null));
	}
	
	@Test
	public void testParseTimeNullDateString_ReturnsDefaultValue() {
		assertEquals(LocalTime.MIN, DataParser.parseTime(null, "HH:mm:ss"));
	}

	@Test
	public void testParseTimeNullParams_ReturnsDefaultValue() {
		assertEquals(LocalTime.MIN, DataParser.parseTime(null, null));
	}
	
	@Test
	public void testParseTimeInvalidFormat_ReturnsDefaultValue() {
		assertEquals(LocalTime.MIN, DataParser.parseTime("02:00", "abcdefg"));
	}
	
	@Test
	public void testParseTimeInvalidTime_ReturnsDefaultValue() {
		assertEquals(LocalTime.MIN, DataParser.parseTime("66:00", "HH:mm"));
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	public void testParseIntValidPositiveInt_ReturnsPrimitiveInt() {
		assertEquals(5,DataParser.parseInteger("5"));
	}
	
	@Test
	public void testParseIntValidNegativeInt_ReturnsPrimitiveInt() {
		assertEquals(-95,DataParser.parseInteger("-95"));
	}
	
	@Test
	public void testParseIntValidSignedPositiveInt_ReturnsPrimitiveInt() {
		assertEquals(195,DataParser.parseInteger("+195"));
	}
	
	// Does not handle scientific notation (potentially a non integer value)
	@Test
	public void testParseIntExponential_ReturnsDefaultValue() { 
		assertEquals(-1,DataParser.parseInteger("9e5"));
	}
	
	@Test
	public void testParseIntNullString_ReturnsDefaultValue() {
		assertEquals(-1,DataParser.parseInteger(null));
	}
	
	@Test
	public void testParseIntNonNumericString_ReturnsDefaultValue() {
		assertEquals(-1,DataParser.parseInteger("-a.5"));
	}
}
