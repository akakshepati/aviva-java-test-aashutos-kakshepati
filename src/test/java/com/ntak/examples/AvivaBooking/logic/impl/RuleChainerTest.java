package com.ntak.examples.AvivaBooking.logic.impl;

import org.junit.Test;

import junit.framework.TestCase;

public class RuleChainerTest extends TestCase {

	RuleChainer dummyChainer;
	
	protected void setUp() throws Exception {
		super.setUp();
		dummyChainer = new RuleChainer();
	}
	
	@Test
	public void testChainRule_AppendsToInternalList() {
		dummyChainer.chainRule(new DummyRule())
					.chainRule(new DummyRule())
					.chainRule(new DummyRule())
					.chainRule(new DummyRule())
					.chainRule(new DummyRule());
		
		assertNotNull(dummyChainer.getRules());
		assertEquals(5, dummyChainer.getRules().size());
	}
	
	@Test
	public void testChainNullRule_NotAppended() {
		dummyChainer.chainRule(null);
		
		assertNotNull(dummyChainer.getRules());
		assertEquals(0, dummyChainer.getRules().size());
	}
	
	@Test
	public void testExecuteRules_FiveInvocations() {
		dummyChainer.chainRule(new DummyRule(true))
		.chainRule(new DummyRule(true))
		.chainRule(new DummyRule(true))
		.chainRule(new DummyRule(true))
		.chainRule(new DummyRule(true));
		
		assertNotNull(dummyChainer.getRules());
		assertTrue(dummyChainer.executeRules(null, null));
		assertEquals(5,DummyRule.getNoOfInvocations());
	}
	
	@Test
	public void testExecuteRules_ThreeInvocations() {
		dummyChainer.chainRule(new DummyRule(true))
		.chainRule(new DummyRule(true))
		.chainRule(new DummyRule())
		.chainRule(new DummyRule())
		.chainRule(new DummyRule());
		
		assertNotNull(dummyChainer.getRules());
		assertFalse(dummyChainer.executeRules(null, null));
		assertEquals(3,DummyRule.getNoOfInvocations());
	}
	
	
	protected void tearDown() throws Exception {
		super.tearDown();
		DummyRule.resetCounter();
		dummyChainer.flush();
	}

}
