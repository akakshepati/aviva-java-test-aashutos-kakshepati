package com.ntak.examples.AvivaBooking.io.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ntak.examples.AvivaBooking.io.exception.DataAccessException;

public class OutputStreamDataWriterTest {

	private OutputStreamDataWriter wtr;
	private FileOutputStream oStream;
	private File outFile;
	
	@Before
	public void setUp() throws Exception {
		outFile = new File("src/test/resource/outFile.txt");
		oStream = new FileOutputStream(outFile);
		wtr = new OutputStreamDataWriter(oStream);
	}

	@After
	public void tearDown() throws Exception {
		wtr.close();
		oStream.close();
		outFile.delete();
	}

	@Test
	public void testConsumeLine() throws DataAccessException {
		wtr.consumeLine("A Line of text.");
	}
	
	@Test
	public void testConsumeLineToSysOut() throws DataAccessException {
		wtr = new OutputStreamDataWriter(System.out);
		wtr.consumeLine("A Line of text to System.Out stream.");
	}
	
	@Test(expected=DataAccessException.class)
	public void testConsumeLineThrowsException() throws IOException, DataAccessException {
		oStream.close();
		wtr.consumeLine("A Line of text.");
	}

}
