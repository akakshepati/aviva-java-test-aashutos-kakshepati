package com.ntak.examples.AvivaBooking.io.impl;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ntak.examples.AvivaBooking.io.exception.DataAccessException;

public class InputStreamDataReaderTest {

	private InputStreamDataReader rdr;
	private InputStream iStream;
	@Before
	public void setUp() throws Exception {
		iStream = new FileInputStream(new File("src/test/resource/testFile.txt"));
		rdr = new InputStreamDataReader(iStream);
	}

	@After
	public void tearDown() throws Exception {
		rdr.close();
		iStream.close();
	}

	@Test
	public void testReadLine() throws DataAccessException {
		assertEquals("TempLine",rdr.readLine());
	}
	
	@Test(expected=DataAccessException.class)
	public void testReadLineCatchException() throws IOException, DataAccessException {
		iStream.close();
		rdr.readLine();
	}
}
