package com.ntak.examples.AvivaBooking.controllers;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ntak.examples.AvivaBooking.io.exception.DataAccessException;
import com.ntak.examples.AvivaBooking.io.impl.InputStreamDataReader;
import com.ntak.examples.AvivaBooking.io.impl.OutputStreamDataWriter;
import com.ntak.examples.AvivaBooking.logic.Rule;
import com.ntak.examples.AvivaBooking.logic.impl.DataParser;
import com.ntak.examples.AvivaBooking.logic.impl.NoMeetingClashRule;
import com.ntak.examples.AvivaBooking.logic.impl.OfficeHoursBoundsRule;
import com.ntak.examples.AvivaBooking.logic.impl.RuleChainer;
import com.ntak.examples.AvivaBooking.model.beans.Meeting;

public class AvivaBookingControllerTest {

	AvivaBookingController controller;
	InputStream ipStream;
	InputStreamDataReader reader;
	OutputStreamDataWriter writer;
	RuleChainer ruleExecutor;
	
	@Before
	public void setUp() throws Exception {
    	ipStream = new FileInputStream(new File("src/main/resource/input.txt"));
    	reader = new InputStreamDataReader(ipStream);
    	writer = new OutputStreamDataWriter(System.out);
    	ruleExecutor = new RuleChainer();
    	
    	String officeHours = reader.readLine();
    	
    	Rule officeHoursRule = new OfficeHoursBoundsRule(DataParser.parseTime(officeHours.split("\\s")[0],"HH:mm"), DataParser.parseTime(officeHours.split("\\s")[1],"HH:mm"));
    	Rule meetingClashRule = new NoMeetingClashRule();
    	
    	ruleExecutor.chainRule(meetingClashRule).chainRule(officeHoursRule);
    	
    	controller = new AvivaBookingController(reader, writer, ruleExecutor);
	}

	@Test
	public void testNormalCase_ReturnsThreeMeetings() {
		assertTrue(controller.execute());
		List<Meeting> output = controller.getMeetings();
		assertEquals(3,output.size());
		assertEquals("09:00 11:00 EMP001",output.get(0).printRecord());
		assertEquals("14:00 16:00 EMP003",output.get(1).printRecord());
		assertEquals("16:00 17:00 EMP004",output.get(2).printRecord());
	}
	
	@Test
	public void testCorruptFile_ReturnsFalse() throws FileNotFoundException, DataAccessException {
    	ipStream = new FileInputStream(new File("src/test/resource/corruptFile.txt"));
    	reader = new InputStreamDataReader(ipStream);
    	writer = new OutputStreamDataWriter(System.out);
    	ruleExecutor = new RuleChainer();
    	
    	String officeHours = reader.readLine();
    	
    	Rule officeHoursRule = new OfficeHoursBoundsRule(DataParser.parseTime(officeHours.split("\\s")[0],"HH:mm"), DataParser.parseTime(officeHours.split("\\s")[1],"HH:mm"));
    	Rule meetingClashRule = new NoMeetingClashRule();
    	
    	ruleExecutor.chainRule(meetingClashRule).chainRule(officeHoursRule);
    	
		
		controller = new AvivaBookingController(reader, writer, ruleExecutor);
		assertFalse(controller.execute());
	}
	
	
	@Test
	public void testNullReader() {
		controller = new AvivaBookingController(null, writer, ruleExecutor);
		assertFalse(controller.execute());
		assertEquals(0,controller.getMeetings().size());
	}

	@Test
	public void testNullWriter() {
		controller = new AvivaBookingController(reader, null, ruleExecutor);
		assertFalse(controller.execute());
		assertEquals(0,controller.getMeetings().size());
	}
	
	@Test
	public void testNullRules() {
		controller = new AvivaBookingController(reader, writer, null);
		assertFalse(controller.execute());
		assertEquals(0,controller.getMeetings().size());
	}
	
	
	@After
	public void tearDown() throws Exception {
		ipStream.close();
		reader.close();
		writer.close();
	}
}
