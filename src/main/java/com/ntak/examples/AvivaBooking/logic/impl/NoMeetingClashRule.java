package com.ntak.examples.AvivaBooking.logic.impl;

import java.util.List;

import com.ntak.examples.AvivaBooking.logic.Rule;
import com.ntak.examples.AvivaBooking.model.beans.Meeting;

/**
 * Rule ensures the new meeting does not overlap with existing meetings.
 * 
 * @author akakshepati
 *
 */
public class NoMeetingClashRule implements Rule {

	@Override
	public boolean checkValid(Meeting newMeet, List<Meeting> meetings) {
		if (newMeet == null)
			return false;
		if (meetings == null || meetings.size() == 0)
			return true;
		for (Meeting meeting : meetings) {
			if (meeting == null 
					|| (newMeet.getStartTime().isAfter(meeting.getStartTime())
						&& !newMeet.getStartTime().isBefore(meeting.getEndTime()))
					|| (newMeet.getEndTime().isBefore(meeting.getEndTime())
						&& !newMeet.getEndTime().isAfter(meeting.getStartTime())
						)
					)
				continue;
			return false;
		}
		return true;
	}

}
