package com.ntak.examples.AvivaBooking.logic.impl;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * A collection of utility methods used to cast read String values into other types/
 * 
 * @author akakshepati
 *
 */
public class DataParser {

	public static LocalDate parseDate(String line, String format) {
		try {
			return LocalDate.parse(line, DateTimeFormatter.ofPattern(format));
		} catch (NullPointerException|IllegalArgumentException|DateTimeParseException e) {
			return LocalDate.MIN;
		}
		
	}
	
	public static LocalTime parseTime(String line, String format) {
		try {
			return LocalTime.parse(line, DateTimeFormatter.ofPattern(format));
		} catch (NullPointerException|IllegalArgumentException|DateTimeParseException e) {
			return LocalTime.MIN;
		}
	}
	
	public static int parseInteger(String line) {
		try {
			return Integer.parseInt(line);
		} catch (NullPointerException|NumberFormatException e) {
			return -1;
		}
	}
	
}
