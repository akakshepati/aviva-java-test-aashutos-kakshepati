package com.ntak.examples.AvivaBooking.logic.impl;

import java.time.LocalTime;
import java.util.List;

import com.ntak.examples.AvivaBooking.logic.Rule;
import com.ntak.examples.AvivaBooking.model.beans.Meeting;

/**
 * Rule defining that meeting should be bounded within the specified opening and closed times. If invalid times are specified the default values are used of 9am to 6pm.
 * 
 * @author akakshepati
 *
 */
public class OfficeHoursBoundsRule implements Rule {

	private final LocalTime openingTime;
	private final LocalTime closingTime;

	public OfficeHoursBoundsRule() {
		this(null,null);
	}
	
	public OfficeHoursBoundsRule(LocalTime openingTime, LocalTime closingTime) {
		super();

		if (openingTime == null || closingTime == null || !closingTime.isAfter(openingTime)) {
			this.openingTime = LocalTime.parse("09:00");
			this.closingTime = LocalTime.parse("18:00");
		} else {
			this.openingTime = openingTime;
			this.closingTime = closingTime;
		}
	}

	@Override
	public boolean checkValid(Meeting newMeet, List<Meeting> meetings) {
		if (newMeet == null)
			return false;

		if (!newMeet.getStartTime().isBefore(openingTime) && !newMeet.getStartTime().isAfter(closingTime) && !newMeet.getEndTime().isAfter(closingTime)) {
			return true;
		}

		return false;
	}

	public LocalTime getOpeningTime() {
		return openingTime;
	}

	public LocalTime getClosingTime() {
		return closingTime;
	}
	
}
