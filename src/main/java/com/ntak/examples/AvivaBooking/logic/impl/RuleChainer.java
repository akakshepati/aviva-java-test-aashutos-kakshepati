package com.ntak.examples.AvivaBooking.logic.impl;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.ntak.examples.AvivaBooking.logic.Rule;
import com.ntak.examples.AvivaBooking.model.beans.Meeting;

/**
 * Wrapper class which contains a collection of rules to validate meetings against.
 * 
 * @author akakshepati
 *
 */
public class RuleChainer {

	private  final List<Rule> rules = new LinkedList<>();
	
	public RuleChainer chainRule(Rule rule) {
		
		if (rule == null)
			return this;
		
		rules.add(rule);
		
		return this;		
	}
	
	public boolean executeRules(Meeting meeting, List<Meeting> meetings) {
		for (Rule rule : rules) {
			if (rule.checkValid(meeting, meetings))
				continue;
			return false;
		}
		return true;
	}

	public List<Rule> getRules() {
		return Collections.unmodifiableList(rules);
	}
	
	public void flush() {
		rules.clear();
	}
	
}
