package com.ntak.examples.AvivaBooking.logic;

import java.util.List;

import com.ntak.examples.AvivaBooking.model.beans.Meeting;

public interface Rule {

	public boolean checkValid(Meeting newMeet, List<Meeting> meetings);
	
}
