package com.ntak.examples.AvivaBooking.model.beans;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Optional;

/**
 * <p> Bean encapsulating meeting information. </p> 
 * 
 * <b>Notes:</b>
 * <p>If invalid input is placed within time fields and duration,
 *  the default values will be used of Start Time = LocalTime.MIDNIGHT
 *   and duration = 1 (hour).
 * </p>
 * 
 * <p>
 * Also, if the SubmissionDate or Date is missing, the method LocalDateTime.now() or LocalDate.now() will be used respectively.
 * </p>
 * 
 * <p>
 * Finally, if a user is not supplied (null) then the user information will be pulled from the System constant: user.name
 * </p>
 * @author akakshepati
 *
 */
public class Meeting implements Comparable<Meeting> {

	private final LocalDate 	date;
	private final LocalTime 	startTime;
	private final LocalTime 	endTime;
	private final int 	  		duration;
	private final String 		employeeId;
	private final LocalDateTime		submissionTime;
	
	public Meeting(LocalDate startDate, LocalTime startTime, int duration, String employeeId,
			LocalDateTime submissionTime) {
		super();
		LocalTime tempStartTime = Optional.ofNullable(startTime).orElse(LocalTime.of(0, 0));
		LocalTime tempEndTime = Optional.ofNullable(startTime).orElse(LocalTime.of(0, 0)).plusHours(duration);
		
		if (!tempEndTime.isAfter(tempStartTime) || duration > 24) {
			this.startTime = LocalTime.of(0, 0);
			this.endTime = this.startTime.plusHours(1);
		} else {
			this.startTime = Optional.ofNullable(startTime).orElse(LocalTime.of(0, 0));
			this.endTime = Optional.ofNullable(startTime).orElse(LocalTime.of(0, 0)).plusHours(duration);
		}
		
		this.date = Optional.ofNullable(startDate).orElse(LocalDate.now());		
		this.duration = duration;
		//startTime.until(LocalTime.MIDNIGHT, ChronoUnit.HOURS);	
		this.employeeId = Optional.ofNullable(employeeId).orElse(System.getProperty("user.name"));
		this.submissionTime = Optional.ofNullable(submissionTime).orElse(LocalDateTime.now());
	}

	public LocalDate getDate() {
		return date;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public int getDuration() {
		return duration;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public LocalDateTime getSubmissionTime() {
		return submissionTime;
	}
	
	public LocalTime getEndTime() {
		return endTime;
	}

	/**
	 * Convenience method for formatting output of meeting for output in the manner: {Start Time - HH:mm} {End Time - HH:mm} EmployeeId
	 * 
	 * @return String: Formatted output representation of meeting
	 */
	public String printRecord() {
		return startTime + " " + endTime + " " + employeeId;		
	}

	/**
	 * Implements Comparable for sorting meetings by date and time. Null Meeting beans are pushed to the end.
	 * 
	 * @return int: -1,0,1 for less than, equals, greater than
	 */
	public int compareTo(Meeting o) {
		if (o == null)
			return -1;
		if (o.date.equals(this.date) && o.startTime.equals(this.startTime))
			return 0;
		
		if (o.date.isAfter(this.date))
			return -1;
		
		if (o.startTime.isAfter(this.startTime))
			return -1;
		else {
			return 1; 
		}
	}
}
