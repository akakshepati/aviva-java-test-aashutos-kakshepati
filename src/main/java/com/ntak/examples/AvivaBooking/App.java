package com.ntak.examples.AvivaBooking;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import com.ntak.examples.AvivaBooking.controllers.AvivaBookingController;
import com.ntak.examples.AvivaBooking.io.exception.DataAccessException;
import com.ntak.examples.AvivaBooking.io.impl.InputStreamDataReader;
import com.ntak.examples.AvivaBooking.io.impl.OutputStreamDataWriter;
import com.ntak.examples.AvivaBooking.logic.Rule;
import com.ntak.examples.AvivaBooking.logic.impl.DataParser;
import com.ntak.examples.AvivaBooking.logic.impl.NoMeetingClashRule;
import com.ntak.examples.AvivaBooking.logic.impl.OfficeHoursBoundsRule;
import com.ntak.examples.AvivaBooking.logic.impl.RuleChainer;

/**
 * Main entry point for the application. It expects one argument: The input file location.
 *
 */
public class App 
{
    public static void main( String[] args ) throws FileNotFoundException, DataAccessException
    {
    	if (args.length == 0 || args[0] == null || args[0].trim().isEmpty()) {
    		System.err.println("Invalid arguments passed in.");
    		return;
    	}
    	
    	InputStream ipStream = new FileInputStream(new File(args[0]));
    	InputStreamDataReader reader = new InputStreamDataReader(ipStream);
    	OutputStreamDataWriter writer = new OutputStreamDataWriter(System.out);
    	RuleChainer ruleExecutor = new RuleChainer();
    	
    	String officeHours = reader.readLine();
    	
    	Rule officeHoursRule = new OfficeHoursBoundsRule(DataParser.parseTime(officeHours.split("\\s")[0],"HH:mm"), DataParser.parseTime(officeHours.split("\\s")[1],"HH:mm"));
    	Rule meetingClashRule = new NoMeetingClashRule();
    	
    	ruleExecutor.chainRule(officeHoursRule).chainRule(meetingClashRule);
    	
    	AvivaBookingController bookingController = new AvivaBookingController(reader, writer, ruleExecutor);
    	boolean success = bookingController.execute();
    	if (success) {
    		bookingController.outputMeetingSchedule();
    	}
    }
}
