package com.ntak.examples.AvivaBooking.io.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.ntak.examples.AvivaBooking.io.DataReader;
import com.ntak.examples.AvivaBooking.io.exception.DataAccessException;

/**
 * Reads data from an input stream in a line-by-line basis.
 * 
 * @author akakshepati
 *
 */
public class InputStreamDataReader implements DataReader {

	private final BufferedReader br;
	
	public InputStreamDataReader(InputStream ipStream) {
		super();
		this.br = new BufferedReader(new InputStreamReader(ipStream));
	}

	@Override
	public String readLine() throws DataAccessException {
		try {
			return br.readLine();
		} catch (IOException e) {
			throw new DataAccessException("IOException caught: Could not read line from input stream. IOException details: " + e.getMessage(),e);
		}
	}


	@Override
	public void close() throws DataAccessException {
			try {
				if (br != null)
					br.close();
			} catch (IOException e) {
				throw new DataAccessException("IOException caught: Could not close input stream. IOException details: " + e.getMessage(),e);
			}
	}

}
