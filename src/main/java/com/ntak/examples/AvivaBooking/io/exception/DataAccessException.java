package com.ntak.examples.AvivaBooking.io.exception;

/**
 * Exception thrown when trying to Read/Write data from various sources
 *  using the DataReader and DataWriter interfaces.
 * 
 * @author akakshepati
 *
 * @see com.ntak.examples.AvivaBooking.io.impl.InputStreamDataReader
 * @see com.ntak.examples.AvivaBooking.io.impl.SystemOutDataWriter
 */
public class DataAccessException extends Exception {

	private Throwable nested;

	private static final long serialVersionUID = -4120641493341634916L;

	public DataAccessException(String message) {
		super(message);
	}
	
	public DataAccessException(String message, Throwable t) {
		this(message);
		nested = t;
	}
	
	public Throwable getNested() {
		return nested;
	}

}
