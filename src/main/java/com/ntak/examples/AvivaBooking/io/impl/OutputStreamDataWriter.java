package com.ntak.examples.AvivaBooking.io.impl;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import com.ntak.examples.AvivaBooking.io.DataWriter;
import com.ntak.examples.AvivaBooking.io.exception.DataAccessException;

/**
 * Wrapper for standard System.Out output stream. 
 * 
 * @author akakshepati
 *
 */
public class OutputStreamDataWriter implements DataWriter {

	private final BufferedWriter bw;
	
	public OutputStreamDataWriter(OutputStream opStream) {
		super();
		bw = new BufferedWriter(new OutputStreamWriter(opStream));
	}
	
	@Override
	public void consumeLine(String line) throws DataAccessException {
		try {
			if (bw == null)
				return;
			bw.write(line);
			bw.write("\r\n");
			bw.flush();
		} catch (IOException e) {
			throw new DataAccessException("IOException caught: Could not write line to output stream. IOException details: " + e.getMessage(),e);
		}
	}

	@Override
	public void close() throws DataAccessException {
		try {
			if (bw != null)
				bw.close();
		} catch (IOException e) {
			throw new DataAccessException("IOException caught: Could not close output stream. IOException details: " + e.getMessage(),e);
		}
	}

}
