package com.ntak.examples.AvivaBooking.io;

import com.ntak.examples.AvivaBooking.io.exception.DataAccessException;

/**
 * Simple Interface, which stipulates that data can be read from some source and formed into String output.
 * 
 * @author akakshepati
 *
 */
public interface DataReader extends AutoCloseable {
	
	public String readLine() throws DataAccessException;
}
