package com.ntak.examples.AvivaBooking.io;

import com.ntak.examples.AvivaBooking.io.exception.DataAccessException;

/**
 * Simple interface, which specifies that a String line can be consumed and processed.
 * 
 * @author akakshepati
 *
 */
public interface DataWriter extends AutoCloseable {

	public void consumeLine(String line) throws DataAccessException;
}
