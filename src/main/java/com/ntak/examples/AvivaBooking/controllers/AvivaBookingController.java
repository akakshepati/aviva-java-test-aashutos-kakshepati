package com.ntak.examples.AvivaBooking.controllers;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import com.ntak.examples.AvivaBooking.io.exception.DataAccessException;
import com.ntak.examples.AvivaBooking.io.impl.InputStreamDataReader;
import com.ntak.examples.AvivaBooking.io.impl.OutputStreamDataWriter;
import com.ntak.examples.AvivaBooking.logic.impl.DataParser;
import com.ntak.examples.AvivaBooking.logic.impl.RuleChainer;
import com.ntak.examples.AvivaBooking.model.beans.Meeting;

/**
 * Controller class, which delegated most of the logic to sub classes
 *  in order to book meetings and present the meeting schedule.
 * 
 * @author akakshepati
 *
 */
public class AvivaBookingController {

	private final InputStreamDataReader reader;
	private final OutputStreamDataWriter writer;
	private final RuleChainer ruleExecutor;
	private final Pattern spaceDelimited = Pattern.compile("\\s");
	private 	  List<Meeting> meetings;
	
	public AvivaBookingController(InputStreamDataReader reader, OutputStreamDataWriter writer,
			RuleChainer ruleExecutor) {
		super();
		this.reader = reader;
		this.writer = writer;
		this.ruleExecutor = ruleExecutor;
		meetings = new LinkedList<>();
	}
	
	public boolean execute() {
		if (reader == null || writer == null || ruleExecutor == null)
			return false;
		
		String lineMeta;
		String lineData;
		try {
			while ((lineMeta = reader.readLine()) != null && (lineData = reader.readLine()) != null) {
				String[] splitLineMeta = spaceDelimited.split(lineMeta);								
				String[] splitLineData = spaceDelimited.split(lineData);
				
				Meeting meeting = new Meeting(DataParser.parseDate(splitLineData[0], "yyyy-MM-dd"),DataParser.parseTime(splitLineData[1], "HH:mm"),DataParser.parseInteger(splitLineData[2]),splitLineMeta[2],DataParser.parseDate(splitLineMeta[0], "yyyy-MM-dd").atTime(DataParser.parseTime(splitLineMeta[1], "HH:mm")));
				meetings.add(meeting);
			}
			
			Collections.sort(meetings,(u,v)->(v.getSubmissionTime().compareTo(u.getSubmissionTime())));
			Iterator<Meeting> iterator = meetings.iterator();
			List<Meeting> meetingsOut = new LinkedList<Meeting>();
			while (iterator.hasNext()) {
				Meeting meeting = iterator.next();
				iterator.remove();
				
				if (ruleExecutor.executeRules(meeting, meetingsOut)){
					meetingsOut.add(meeting);
				} else {
					System.err.println("Meeting: " + meeting.printRecord() + " is not valid.");
				}
			}
			
			meetings = meetingsOut;
			
		} catch (DataAccessException e) {
			System.err.println("Malformed meeting entry detected.");
			return false;
		} catch (ArrayIndexOutOfBoundsException e) {
			System.err.println("Malformed input detected.");
			return false;
		}
		return true;
	}

	public List<Meeting> getMeetings() {
		Collections.sort(meetings);
		return Collections.unmodifiableList(meetings);
	}
	
	public void outputMeetingSchedule() {
		Collections.sort(meetings);
		LocalDate currDate = null;
		for (Meeting meeting : meetings) {
			try {
				if (currDate == null || !meeting.getDate().equals(currDate)){
					currDate = meeting.getDate();
					writer.consumeLine(currDate.toString());					
				}
				writer.consumeLine(meeting.printRecord());
			} catch (DataAccessException e) {
				System.err.println("Issue writing meeting information to output.");
				return;
			}
		}
	}
}
